package de.oio.example;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import redis.clients.jedis.Jedis;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	
	@Autowired
	private StringRedisTemplate rtemplate;

	@RequestMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name)throws IOException {
		ContainerEnv ce = new ContainerEnv();
		String host = ce.get("HOSTNAME").getValue();
		Jedis jedis = new Jedis("redisdb");
		  //adding a new key
		  jedis.set(name, host);
		  //getting the key value
		  System.out.println(jedis.get("key"));

	    
		return new Greeting(counter.incrementAndGet(), String.format(template, name+" from "+host +" with-Updates"));
	}
	
	
}