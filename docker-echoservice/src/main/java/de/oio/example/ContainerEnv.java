package de.oio.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class ContainerEnv {

	private HashMap<String, EnviromentValue> envMap= new HashMap<String, EnviromentValue>();
	
	public ContainerEnv() throws IOException {
	Map<String, String> env = System.getenv();
		
		for (String envName : env.keySet()) {
			if(envName.endsWith("_FILE")){
				this.put(envName, new EnviromentValue(loadSecretFile(env.get(envName))));
			}else{
				this.put(envName, new EnviromentValue(env.get(envName)));
			}
			
			
		}
	}
	
	private String loadSecretFile(String path) throws IOException{       
        return new String(Files.readAllBytes(Paths.get(path))); 
	}
	
	public void put(String key,EnviromentValue value){
		envMap.put(key, value);
	}
	
	public EnviromentValue get(String key){
		return this.envMap.get(key);	
	}
	
	@Override
	public String toString() {
		
	StringBuffer sb = new StringBuffer();
	for(Map.Entry<String, EnviromentValue> entry : envMap.entrySet()) {
		sb.append(entry.getKey());
	    sb.append("=");
	    sb.append(entry.getValue().getValue());
	    sb.append('\n');
	}
	
	return sb.toString();
	}
}
