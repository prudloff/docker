package de.oio.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GreetingResponse {

    private  long id;
    private String content;

    public GreetingResponse() {
		// TODO Auto-generated constructor stub
	}
    
    public GreetingResponse(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
		this.content = content;
	}
    public void setId(long id) {
		this.id = id;
	}
    @Override
    public String toString() {
    	return "GreetingResponse{"+"id:"+getId()+", content:'"+getContent()+"'}";
    }
}
