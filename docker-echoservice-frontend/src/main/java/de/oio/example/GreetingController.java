package de.oio.example;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GreetingController {

	private static final String template = "Name, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping("/greetingfrontend")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name)throws IOException {
		ContainerEnv ce = new ContainerEnv();
		String host = ce.get("HOSTNAME").getValue();
		RestTemplate restTemplate = new RestTemplate();
	    GreetingResponse quote = restTemplate.getForObject("http://echo:8080/greeting?name=Hans-"+name, GreetingResponse.class);
		return new Greeting(quote.getId(), "Redirect-From:"+host+". "+quote.getContent());
	}
}